//
//  SudokuTile.h
//  Sudoku
//
//  Created by Anthony on 19/08/06.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>

#define kNumberPossibilities 9

@interface SudokuTile : NSObject {
	int		_x;
	int		_y;
	int		_value;
	BOOL	_isPredefined;
	BOOL	_isDefinitive;
	BOOL	_isIncorrect;
	BOOL    _possibilities[kNumberPossibilities+1];   //9+1 pour commencer les indices � 1
}

+(id)tileAtX:(int)x andY:(int)y;
+(id)tileWithValue:(int)aValue atX:(int)x andY:(int)y;
+(id)tileWithValue:(int)aValue andIsPredefined:(BOOL)isPredefined atX:(int)x andY:(int)y;
+(id)tileWithValue:(int)aValue andIsDefinitive:(BOOL)isDefinitive atX:(int)x andY:(int)y;
+(id)tileWithValue:(int)aValue isDefintive:(BOOL)isDefinitive andIsPredefined:(BOOL)isPredefined atX:(int)x andY:(int)y;

-(id)initWithValue:(int)aValue isDefintive:(BOOL)isDefinitive andIsPredefined:(BOOL)isPredefined atX:(int)x andY:(int)y;

-(int)x;
-(int)y;
-(int)value;
-(BOOL)isPredefined;
-(BOOL)isDefinitive;
-(BOOL)isIncorrect;
-(NSArray *)possibilities;

-(void)insertValue:(int)aValue;
-(void)insertValue:(int)aValue andIsDefinitive:(BOOL)isDefinitive;
-(void)insertValue:(int)aValue andIsPredefined:(BOOL)isPredefined;
-(void)insertValue:(int)aValue isDefinitive:(BOOL)isDefinitive andIsPredefined:(BOOL)isPredefined;
-(void)setValue:(int)aValue;
-(void)setValue:(int)aValue andIsDefinitive:(BOOL)isDefinitive;
-(void)setValue:(int)aValue andIsPredefined:(BOOL)isPredefined;
-(void)setValue:(int)aValue isDefinitive:(BOOL)isDefinitive andIsPredefined:(BOOL)isPredefined;
-(void)setPredefined:(BOOL)isPredefined;
-(void)setDefinitive:(BOOL)isDefinitive;
-(void)setIncorrect:(BOOL)isIncorrect;

-(void)removePossibilitesOf:(int)value;
-(BOOL)containsValue:(int)aValue;
@end
