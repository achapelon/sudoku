#import "SelectionRingView.h"
#import "SudokuGridView.h"

@implementation SelectionRingView
- (id)initWithFrame:(NSRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
		draftMode = NO;
		NSString *image = (draftMode) ? @"stylo.png" : @"crayon.png";
		NSString *imagePath = [[NSBundle mainBundle] pathForImageResource:image];
		if (imagePath == nil) {
			NSRunAlertPanel(@"Alert", [NSString stringWithFormat:@"Image file missing from main bundle: %@", image], @"Quit", nil, nil);
			[[NSApplication sharedApplication] terminate:self];
		}
		[selectionModeButton setImage:[[NSImage alloc] initWithContentsOfFile:imagePath]];
		
		
    }
    return self;
}

- (IBAction)click:(id)sender {
    NSString *candidate = [sender title];
	NSLog(@"%@", candidate);
	
	SudokuGridView *superview = (SudokuGridView *)[self superview];
	[superview addCandidate:[[sender title] intValue] inDraftMode:draftMode];
	[self removeFromSuperview];
	[superview setNeedsDisplay:YES];
}

- (IBAction)toggleMode:(id)sender {
	draftMode = !draftMode;
	
	NSString *image = (draftMode) ? @"stylo.png" : @"crayon.png";
	NSString *imagePath = [[NSBundle mainBundle] pathForImageResource:image];
	if (imagePath == nil) {
		NSRunAlertPanel(@"Alert", [NSString stringWithFormat:@"Image file missing from main bundle: %@", image], @"Quit", nil, nil);
		[[NSApplication sharedApplication] terminate:self];
	}
	[selectionModeButton setImage:[[NSImage alloc] initWithContentsOfFile:imagePath]];
}

@end
