//
//  SudokuGridView.m
//  SudokuResolver
//
//  Created by Anthony on 05/08/06.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import "SudokuGridView.h"

#define kSudokuImageName @"SudokuGrid.tiff"

@implementation SudokuGridView

- (id)initWithFrame:(NSRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
		NSString *imagePath = [[NSBundle mainBundle] pathForImageResource:kSudokuImageName];
		if (imagePath == nil) {
			NSRunAlertPanel(@"Alert", [NSString stringWithFormat:@"Image file missing from main bundle: %@", kSudokuImageName], @"Quit", nil, nil);
			[[NSApplication sharedApplication] terminate:self];
		}
		sudokuImage = [[NSImage alloc] initWithContentsOfFile:imagePath];
		candidateSelectionPending = NO;
		buildMode = YES;
		
		//[delegate createTiles];
		//[self showAllTiles];
    }
    return self;
}


- (void)initTiles {
	int i,j;
	//tiles = [NSMutableArray array];
	for (i=0; i<kTileGridSize; i++) {
		for (j=0; j<kTileGridSize; j++) {
			SudokuTile	*newTile = [SudokuTile tileAtX:i andY:j];
			//[tiles addObject:newTile];
			//[newTile release];
			tiles2D[i][j] = newTile;
		}
	}
	//SudokuTile *aTile = [tiles objectAtIndex:1];
}

- (BOOL)initTilesFromModel {
	id fetchedTile;
	// Fetch the set of tiles
	[self releaseTiles];
	NSLog(@"Fetching all tiles from model...");
    NSFetchRequest *request = [[delegate managedObjectModel] fetchRequestTemplateForName:@"allTiles"];
    NSError *fetchError;
	//NSLog(@"Execute fetch request : %@", request);
	NSArray *fetchedTiles = [[delegate managedObjectContext] executeFetchRequest:request error:&fetchError];
	if ([fetchedTiles count]>0) {
		NSLog(@"Caching tiles...");
		NSEnumerator *enumerator = [fetchedTiles objectEnumerator];
		while (fetchedTile=[enumerator nextObject]) {
			int value = [[fetchedTile valueForKey:@"value"] intValue];
			int x = [[fetchedTile valueForKey:@"xPosition"] intValue];
			int y = [[fetchedTile valueForKey:@"yPosition"] intValue];
			BOOL isPredefined = [[fetchedTile valueForKey:@"isPredefined"] boolValue];;
			SudokuTile *newTile = [SudokuTile tileWithValue:value andIsPredefined:isPredefined atX:x andY:y];
			tiles2D[x][y] = newTile;
		} 
		return YES;
	} else {
		NSLog(@"No tiles to cache...");
		[self initTiles];
		return NO;
	}
}


- (NSArray *)arrayFromTiles {
	int x, y;
	x=y=0;
	NSMutableArray *tiles = [NSMutableArray array];
	while (x<kTileGridSize) {
		y=0;
		while (y<kTileGridSize) {
			SudokuTile *aTile = tiles2D[x][y];
			[tiles addObject:aTile];
			[aTile release];
			y++;
		}
		x++;
	}
	return tiles;
}

/**
Intercept mouse down event
 */
- (void) mouseDown:(NSEvent *)theEvent {
	NSPoint loc = [theEvent locationInWindow];
	/*
	NSPoint idx = [self indexForTileAtPoint:loc];
	int value = [[delegate valueForKey:@"numberChoosen"] intValue];
	BOOL isDefinitive = ![[delegate valueForKey:@"isAPossibility"] boolValue];
	BOOL isPredefined = [[delegate valueForKey:@"isPredefined"] boolValue];
	isDefinitive = (isPredefined==YES) ? isPredefined : isDefinitive;
	SudokuTile *aTile = tiles2D[(int)idx.x][(int)idx.y];
	[aTile insertValue:value isDefinitive:isDefinitive andIsPredefined:isPredefined];
	NSLog(@"Inserted tile : %@", aTile);
	*/
	//START V2
	clickedTile = [self indexForTileAtPoint:loc];
	if ( candidateSelectionPending ) {
		[selectionRingView removeFromSuperview];
		candidateSelectionPending = NO;
	}
	[self addSubview:selectionRingView];
	[selectionRingView setFrameOrigin:NSMakePoint(loc.x-80/2, loc.y-80/2)];
	candidateSelectionPending = YES;
	//END V2
	
	[self setNeedsDisplay:YES];
}

- (void)addCandidate:(int)candidate inDraftMode:(BOOL)draftMode {
	BOOL isDefinitive = !draftMode;
	BOOL isPredefined = buildMode;
	isDefinitive = (isPredefined==YES) ? isPredefined : isDefinitive;
	SudokuTile *aTile = tiles2D[(int)clickedTile.x][(int)clickedTile.y];
	[aTile insertValue:candidate isDefinitive:isDefinitive andIsPredefined:isPredefined];
	NSLog(@"Inserted tile : %@", aTile);
	
}

- (NSRect) rectForTileAtX:(int)tileX andY:(int)tileY {
	//NSPoint tile = [self indexForTileAtPoint:loc];
	
	//NSSize imageSize = [puzzleImage size];
	float pieceWidth = NSWidth([self bounds]) / kTileGridSize;
	float pieceHeight = NSHeight([self bounds]) / kTileGridSize;
	
	NSRect rect = NSMakeRect(tileX * pieceWidth, tileY * pieceHeight, pieceWidth, pieceHeight);
	
	//NSLog(@"Rect for tile %@",rect);
	
    // This rect represents the part of the puzzleImage that this tile will use when drawing this tile
	return rect;
}

- (NSPoint) indexForTileAtPoint:(NSPoint)loc {
	// Figure out which tile is at loc point
	float viewWidth = NSWidth([self bounds]);
	float viewHeight = NSHeight([self bounds]);
	NSPoint locationInView = [self convertPoint:loc fromView:nil];
	
	int tileX = (int)(locationInView.x / viewWidth  * kTileGridSize);
	tileX = tileX > (kTileGridSize - 1) ? (kTileGridSize - 1) : tileX;
	int tileY = (int)(locationInView.y / viewHeight * kTileGridSize);
	tileY = tileY > (kTileGridSize - 1) ? (kTileGridSize - 1) : tileY;
	
	//NSLog(@"Index for tile {i = %d,j = %d}", tileX, tileY);
	
	return NSMakePoint(tileX,tileY);
}

- (void)drawRect:(NSRect)rect {

	//[self showAllTiles];
	// Fill the view with a background color
    [[NSColor whiteColor] set];
    [NSBezierPath fillRect:rect];
	
	//simply draw the grid
	//NSImage *sudokuImage = [delegate valueForKey:@"sudokuImage"];
    //NSAssert(sudokuImage != nil, @"[delegate valueForKey:@\"sudokuImage\"] returned nil!");
	NSImageRep * imgRep = [sudokuImage bestRepresentationForDevice:nil];
    [imgRep drawInRect:rect];

	/*
	NSEvent* theEvent = [NSApp currentEvent];
	NSPoint point = [self indexForTileAtPoint:[theEvent locationInWindow]];
	NSString *str = [NSString stringWithFormat:@"{%0.f,%0.f}", point.x, point.y];
	[str drawInRect:[self rectForTileAtX:point.x andY:point.y] withAttributes:nil];
	*/

    NSSize tileSize;
    tileSize.height = rect.size.height / kTileGridSize;
    tileSize.width  = rect.size.width / kTileGridSize;
	
    // Draw each tile in the appropriate rect
    NSPoint tileOrigin = rect.origin;
    int x, y;
    for (x = 0; x < kTileGridSize; ++x) {
        for (y = 0; y < kTileGridSize; ++y) {
			if ([tiles2D[x][y] value]>0) {
				NSRect gridRect = NSMakeRect(tileOrigin.x, tileOrigin.y, tileSize.width, tileSize.height);
				NSRect tileRect = NSInsetRect(gridRect, 2, 2);
				[self drawTileAtX:x andY:y inRect:tileRect];
			} //else => no need to draw this tile
            tileOrigin.y += tileSize.height;
        }
        tileOrigin.x += tileSize.width;
        tileOrigin.y = rect.origin.y;
    }
}

- (void)drawTileAtX:(int)xPosition andY:(int)yPosition inRect:(NSRect)rect {

	SudokuTile *aTile = tiles2D[xPosition][yPosition];
	//NSManagedObject *fetchedTile = [self fetchTileAtX:xPosition andY:yPosition];
	//int value = [[fetchedTile valueForKey:@"value"] intValue];	
    int value = [aTile value];
	float size = 10;
	NSTextAlignment align = NSRightTextAlignment;
	NSColor *foregroundColor = [NSColor blueColor];
	if ([aTile isPredefined])
		foregroundColor = [NSColor blackColor];
	else if ([aTile isIncorrect])
		foregroundColor = [NSColor redColor];
	
	if ([aTile isDefinitive]) {
		size = rect.size.height/1.2;
		align = NSCenterTextAlignment;
	} else {
		int i;
		i = 1;
	}
	
	NSString *str = [NSString stringWithFormat:@"%d", value];
	NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];
	[style setAlignment:align];
	NSFont *font = [NSFont userFontOfSize:size];
	NSDictionary *dic = [NSDictionary dictionaryWithObjects: [NSArray arrayWithObjects: foregroundColor, font, style, nil]
									  forKeys: [NSArray arrayWithObjects: NSForegroundColorAttributeName, NSFontAttributeName, NSParagraphStyleAttributeName, nil]];
	
	[str drawInRect:rect withAttributes:dic];
	[style release];
}

- (NSPoint) indexRegionForTileAtX:(int)xPosition andY:(int)yPosition {
	return NSMakePoint([self indexRegionForIndex:xPosition],[self indexRegionForIndex:yPosition]);
}

- (int) indexRegionForIndex:(int)index {
	int region;
	
	switch (index) {
	case 0:
	case 1:
	case 2:
		region = 0;
		break;
	case 3:
	case 4:
	case 5:
		region = 1;
		break;
	case 6:
	case 7:
	case 8:
		region = 2;
		break;
	}
	
	return region;
}

- (BOOL)shouldInsertTileValue:(int)value atX:(int)xPosition andY:(int)yPosition {
	BOOL canInsert=YES;
	NSPoint reg = [self indexRegionForTileAtX:xPosition andY:yPosition];
	int x, y;
	
	for (x=3*reg.x; canInsert && x<3*reg.x+3; x++) { //Is the value in the region
		for (y=3*reg.y; canInsert && y<3*reg.y+3; y++) {
			SudokuTile *aTile = tiles2D[x][y];
			if (!(x==xPosition && y==yPosition) && [aTile isDefinitive] && [aTile value]==value)
				canInsert=NO;
		}
	}
	
	for (x=0; canInsert && x<kTileGridSize; x++) { //Is the value in the row
		SudokuTile *aTile = tiles2D[x][yPosition];
		if (x!=xPosition && [aTile isDefinitive] && [aTile value]==value)
			canInsert=NO;
	}
	
	for (y=0; canInsert && y<kTileGridSize; y++) { //Is the value in the column
		SudokuTile *aTile = tiles2D[xPosition][y];
		if (y!=yPosition && [aTile isDefinitive] && [aTile value]==value)
			canInsert=NO;
	}
	
	return canInsert;
}

- (id)fetchTileAtX:(int)xPosition andY:(int)yPosition {
	
    NSDictionary *substitutionVars = [NSDictionary dictionaryWithObjectsAndKeys: [NSNumber numberWithInt:xPosition], @"x",
		[NSNumber numberWithInt:yPosition], @"y", nil];
    NSFetchRequest *request = [[delegate managedObjectModel] fetchRequestFromTemplateWithName:@"tileAtXAndY"
																		substitutionVariables:substitutionVars];
    NSAssert(request != nil, @"fetchRequestFromTemplateWithName:\"tileAtXAndY\" returned nil!");
    
    return [self executeTileFetch:request];
}

- (id)executeTileFetch:(NSFetchRequest *)request {
    // Fetch the tile at xPosition, yPosition using an NSFetchRequest
    // Retrieve the fetch request from the model. This fetch request was defined using the XCode Data Modeler
    // and can be viewed in the "Fetch Requests" list for the "Tile" entity.
	
    NSManagedObjectContext *context = [delegate managedObjectContext];
    NSAssert(context != nil, @"The TilesView's delegate's managedObjectContext is nil!.");
    NSError *fetchError;
	NSArray *fetchedTiles = [context executeFetchRequest:request error:&fetchError];
    if (fetchedTiles == nil) {
        [[NSApplication sharedApplication] presentError:fetchError];
        [[NSApplication sharedApplication] terminate:self];
    }
    NSAssert([fetchedTiles count] == 1, ([NSString stringWithFormat:@"Tile fetch returned %d results!", [fetchedTiles count]]));
	return [fetchedTiles objectAtIndex:0];
}

- (void)insertTileValue:(int)value isDefinitive:(BOOL)isDefinitive andIsPredefined:(BOOL)isPredefined atX:(int)xPosition andY:(int)yPosition {
	SudokuTile *aTile = tiles2D[xPosition][yPosition];
	int _value = [aTile value];
	//NSManagedObjectContext *context = [delegate managedObjectContext];
	//NSManagedObject *fetchedTile = [self fetchTileAtX:xPosition andY:yPosition];
	//int _value = [[fetchedTile valueForKey:@"value"] intValue];
	//BOOL isAPossibility = [[delegate valueForKey:@"isAPossibility"] boolValue];
	//BOOL isPredefined = [[delegate valueForKey:@"isPredefined"] boolValue];
	//BOOL isDefinitive = NO;
	
	if (isDefinitive || isPredefined) { //(!isAPossibility || value==0)
		_value = value;
		//if (value!=0) isDefinitive = YES;
	} else {
		NSString *str = [NSString stringWithFormat:@"%d", _value];
		NSRange range = [str rangeOfString:[NSString stringWithFormat:@"%d", value]];
		NSRange stringNotFound = NSMakeRange(NSNotFound,0);
		if ( NSEqualRanges(range,stringNotFound) )
			 str = [NSString stringWithFormat:@"%d%d", _value, value];
		_value = [str intValue];
	}
	[aTile setValue:_value isDefinitive:isDefinitive andIsPredefined:isPredefined];
}

- (IBAction)check:sender {
	int i,j;
	
	for (i=0; i<kTileGridSize; i++) {
		for (j=0; j<kTileGridSize; j++) {
			SudokuTile *aTile = tiles2D[i][j];
			if (![aTile isPredefined] && ![self shouldInsertTileValue:[aTile value] atX:i andY:j]) 
				[aTile setIncorrect:YES];
		}
	}
	
	[self setNeedsDisplay:YES];
}

- (IBAction)play:sender {
	buildMode = NO;
}

- (IBAction)reset:sender {
	int i,j;
	
	for (i=0; i<kTileGridSize; i++) {
		for (j=0; j<kTileGridSize; j++) {
			SudokuTile *aTile = tiles2D[i][j];
			if (![aTile isPredefined]) [aTile setValue:0];
		}
	}
	
	[self setNeedsDisplay:YES];
}

- (IBAction)showSolution:sender {
	int pass = 0, resolved = 0, resolved2 = 0;
	NSLog(@"----------------------------------");
	NSLog(@"Starting to solve this grid...");
	NSLog(@"----------------------------------");
	
	[self reset:sender]; //remove all user defined tiles
	
	do {
		pass++;
		resolved2 = resolved;
		//In most case this method is enough
		resolved = [self resolveBySearchingCandidates];
		NSLog(@"----------------------------------");
		NSLog(@"Pass %d: %d tiles completed\n\n", pass, resolved);
		if (resolved<kTileGridSize*kTileGridSize) //If the first method is not enough to solve sudoku
			resolved = [self resolveBySearchingPairs]; //try second method
		
		if (resolved<kTileGridSize*kTileGridSize && resolved-resolved2==0) {
			NSLog(@"----------------------------------");
			NSLog(@"This grid cannot be solved");
		}
		
	} while(pass<kTileGridSize*kTileGridSize && resolved-resolved2>0 && resolved<kTileGridSize*kTileGridSize);
	
	[self setNeedsDisplay:YES];
}

-(void)removePossibilitiesForAllTiles {
	int i,j;
	//tiles = [NSMutableArray array];
	for (i=0; i<kTileGridSize; i++)
		for (j=0; j<kTileGridSize; j++) {
			SudokuTile *aTile = tiles2D[i][j];
			if (![aTile isDefinitive]) [aTile setValue:0];
		}
}

- (void)findAllPossibilitiesForTileAtX:(int)xPosition andY:(int)yPosition update:(int *)resolved {
	SudokuTile *aTile = tiles2D[xPosition][yPosition];
	int valueTile = [aTile value];
	if (valueTile==0 || valueTile>kTileGridSize) { //we have to find possibilities for this tile
		int value;
		id number;
		NSEnumerator *enumerator = [[aTile possibilities] objectEnumerator];
		[aTile setValue:0];
		while (number=[enumerator nextObject]) { 
			value = [number intValue];
			if ([self shouldInsertTileValue:value atX:xPosition andY:yPosition])
				[aTile insertValue:value];
				valueTile = [aTile value];
		}
	}
	if (valueTile>0 && valueTile<=kTileGridSize  && ![aTile isDefinitive]) {
		(*resolved)++;
		[aTile setDefinitive:YES];
	}
}

- (int)resolveByUnicityOfCandidate {
	int x,y,n,resolved = 0;
	
	for(x=0; x<kTileGridSize; x++) {
		for(y=0; y<kTileGridSize; y++) {
			for (n=1; n<+kTileGridSize; n++) {
				SudokuTile *aTile = tiles2D[x][y];
				if (![aTile isDefinitive]) {
					if ([self shouldValidateTileValue:n atX:x andY:y]) {
						resolved++;
						[aTile setValue:n andIsDefinitive:YES];	
					}
				}
			}
		}
	}
	
	NSLog(@"%d resolved for unicity of candidate", resolved);
	return resolved;
}

- (int)resolveBySearchingCandidates {
	int x, y, n, ncount,resolved_row,resolved_col,resolved,resolved_uni,resolved_unicity,pass_row,pass_col,pass,total_uniq_tiles;
	BOOL hasBeenValidated;
	resolved_row=resolved_col=resolved_uni=resolved=pass_row=pass_col=pass=0;
	total_uniq_tiles = 0;
	do {
		pass++;
		resolved = 0;
		do { //we loop until we resolved tiles
			[self removePossibilitiesForAllTiles]; //?
			resolved += resolved_col;
			pass_col++;
			resolved_col=0;
			for (x=0; x<kTileGridSize; x++) { //we loop over all columns
				for (n=1; n<=kTileGridSize; n++) { //we loop over all values
					ncount = 0; //count how many time this value has been inserted in the row
					for (y=0; y<kTileGridSize; y++) { //we loop over all rows
						SudokuTile *aTile = tiles2D[x][y];
						//int valueTile = [aTile value];
						if (![aTile isDefinitive]) { //we look only tiles with no possibilities
							if ([self shouldInsertTileValue:n atX:x andY:y]) {
								ncount++;
								[aTile insertValue:n];
							}
						}
					}
					if (ncount==1) { //the value has been inserted one time => we can put the value where it can be inserted
						hasBeenValidated = NO;
						for (y=0; !hasBeenValidated && y<kTileGridSize;y++) { //we loop over the column's tiles to set the value inserted to definitive
							SudokuTile *aTile = tiles2D[x][y];
							BOOL isDefinitive = [aTile isDefinitive];
							if ([aTile containsValue:n] && !isDefinitive) {
								resolved_col++;
								hasBeenValidated = YES;
								[aTile setValue:n andIsDefinitive:YES];
							}
						}
					}
				}
			} //end of looping all tiles
			NSLog(@"Columns pass %d: %d solved by column", pass_col, resolved_col);
		} while (resolved_col>0);
		
		do { //we loop until we resolved tiles
			[self removePossibilitiesForAllTiles]; //?
			resolved += resolved_row;
			pass_row++;
			resolved_row=0;
			for (y=0; y<kTileGridSize; y++) { //we loop over all rows
				for (n=1; n<=kTileGridSize; n++) { //we loop over all values
					ncount = 0; //count how many time this value has been inserted in the row
					for (x=0; x<kTileGridSize; x++) { //we loop over all colums
						SudokuTile *aTile = tiles2D[x][y];
						//int valueTile = [aTile value];
						if (![aTile isDefinitive]) { //we look only tiles with no possibilities
							if ([self shouldInsertTileValue:n atX:x andY:y]) {
								ncount++;
								[aTile insertValue:n];
							}
						}
					}
					if (ncount==1) { //the value has been inserted one time => we can set the value to definitive
						hasBeenValidated = NO;
						for (x=0; !hasBeenValidated && x<kTileGridSize;x++) { //we loop over the row's tiles to set the value inserted to definitive
							SudokuTile *aTile = tiles2D[x][y];
							BOOL isDefinitive = [aTile isDefinitive];
							if ([aTile containsValue:n] && !isDefinitive) {
								resolved_row++;
								hasBeenValidated = YES;
								[aTile setValue:n andIsDefinitive:YES];
							}
						}
					}
				}
			} //end of looping all rows
			NSLog(@"Rows pass %d: %d solved by row", pass_row, resolved_row);
		} while (resolved_row>0);
		
		resolved_uni = [self resolveByUnicityOfPossibility:&total_uniq_tiles];
		resolved_unicity=0;
		if (total_uniq_tiles<kTileGridSize*kTileGridSize)
			resolved_unicity = [self resolveByUnicityOfCandidate];
		
		resolved += resolved_uni + resolved_unicity;
		NSLog(@"----------------------------------");
		NSLog(@"Pass %d: %d solved at all\n\n", pass, resolved);
		pass_row=pass_col=0;
	} while (pass<kTileGridSize*kTileGridSize && total_uniq_tiles<kTileGridSize*kTileGridSize && resolved>0);
	
	return total_uniq_tiles;
}

- (int)resolveBySearchingPairs {
	int x, y, location[3], pass = 0, removed = 0, resolved, total_uniq_tiles;
	int pairsFound = 0, pairsFound2 = 0;
	
	do {
		pairsFound2 = pairsFound;
		pairsFound = 0;
		removed = 0;
		pass++;
		for (x=0; x<kTileGridSize; x++) {
			for (y=0; y<kTileGridSize; y++) {
				SudokuTile *aTile = tiles2D[x][y];
				int value = [aTile value];
				if ([self containsPairOfPossibilities:value atX:x andY:y location:location]) {
					pairsFound++;
					removed += [self removeFromTilesPossibilitiesOfPair:value  atX:x andY:y inLocation:location];
				}
			}
		}
		NSLog(@"Pass %d : %d pairs found and %d possibilities removed", pass, pairsFound, removed);
	} while (pairsFound-pairsFound2>0 && pass<81);
	
	resolved = total_uniq_tiles = 0;
	for (x=0; x<kTileGridSize; x++) {
		for (y=0; y<kTileGridSize; y++) {
			SudokuTile *aTile = tiles2D[x][y];
			int value = [aTile value];
			if (value>0 && value<=kTileGridSize) {
				total_uniq_tiles++;
				if (![aTile isDefinitive]) {
					resolved++;
					[aTile setDefinitive:YES];
					[self removePossibilityByReferenceOf:value atX:x andY:y];
				}
			}
		}
	}
	
	[self resolveByUnicityOfPossibility];
	
	int solved;
	pass = 0;
	do {
		pass++;
		solved = 0;
		for (x=0; x<kTileGridSize; x++) {
			for (y=0; y<kTileGridSize; y++) {
				[self findAllPossibilitiesForTileAtX:x andY:y update:&solved];
			}
		}
		NSLog(@"Pass %d : %d solved", pass, solved);
	} while (solved>0);
	
	return solved+resolved;
}

- (void)removePossibilityByReferenceOf:(int)value atX:(int)xPosition andY:(int)yPosition {
	NSPoint reg = [self indexRegionForTileAtX:xPosition andY:yPosition];
	int x, y;
	int removed_reg,removed_row,removed_col,removed;
	
	removed_reg = 0;
	for (x=3*reg.x; x<3*reg.x+3; x++) { //Is the value in the region
		for (y=3*reg.y; y<3*reg.y+3; y++) {
			SudokuTile *aTile = tiles2D[x][y];
			if (![aTile isDefinitive]) {
				[aTile removePossibilitesOf:value];
			}
		}
	}
	
	removed_row = 0;
	for (x=0; x<kTileGridSize; x++) { //Is the value in the row
		SudokuTile *aTile = tiles2D[x][yPosition];
		if (![aTile isDefinitive]) {
			[aTile removePossibilitesOf:value];
		}
	}
	
	removed_col = 0;
	for (y=0; y<kTileGridSize; y++) { //Is the value in the column
		SudokuTile *aTile = tiles2D[xPosition][y];
		if (![aTile isDefinitive]) {
			[aTile removePossibilitesOf:value];
		}
	}
	removed = removed_reg + removed_row + removed_col;
/*	
	resolved = [self resolveByUnicityOfCandidate];

	if (resolved>0) {
		for (x=0; x<kTileGridSize; x++) {
			for (y=0; y<kTileGridSize; y++) {
				SudokuTile *aTile = tiles2D[x][y];
				int value = [aTile value];
				if (value>0 && value<=kTileGridSize)
					;//[self removePossibilityByReferenceOf:value atX:x andY:y];
			}
		}
	}
*/
}

- (void)prepareForResolution {
	int x,y;
	
	for (x=0; x<kTileGridSize; x++) {
		for (y=0; y<kTileGridSize; y++) {
			SudokuTile *aTile = tiles2D[x][y];
			if ([aTile value]==0) 
				[aTile setDefinitive:NO];
		}
	}
	
}

- (int)resolveByUnicityOfPossibility {
	int total;
	return [self resolveByUnicityOfPossibility:&total];
}

- (int)resolveByUnicityOfPossibility:(int *)totalCompleted {
	int x,y,resolved = 0;
	(*totalCompleted) = 0;
	for (x=0; x<kTileGridSize; x++) {
		for (y=0; y<kTileGridSize; y++) {
			SudokuTile *aTile = tiles2D[x][y];
			int value = [aTile value];
			if (value>0 && value<=kTileGridSize) {
				(*totalCompleted)++;
				if (![aTile isDefinitive]) {
					resolved++;
					[aTile setDefinitive:YES];
				}
			}
		}
	}
	
	NSLog(@"%d resolved for unicity of possibility", resolved);
	return resolved;
}

- (BOOL)containsPairOfPossibilities:(int)value atX:(int)xPosition andY:(int)yPosition location:(int *)location {
	BOOL containsPair=NO;
	NSPoint reg = [self indexRegionForTileAtX:xPosition andY:yPosition];
	SudokuTile *maTile = tiles2D[xPosition][yPosition];
	NSArray *possibilities = [maTile possibilities];
	int i, x, y;
	int countPossibility = 0, countPair = 0, passNeed = [possibilities count]; //count of pair, number of possibilities in pair
	
	
	//Remove previous locations
	for (x=0; x<3; x++)
		location[x]=0;
			
	if (passNeed>1 && passNeed<4) { //if the pair has more than 3 possibilities => no need to search for other pair
		countPair = 1;
		for (x=3*reg.x; x<3*reg.x+3; x++) { //Is the pair in the region
			for (y=3*reg.y; y<3*reg.y+3; y++) {
				SudokuTile *aTile = tiles2D[x][y];
				int nrOfPossibitiesToCheck = [[aTile possibilities] count];
				BOOL shouldCompare = (nrOfPossibitiesToCheck<=passNeed);
				if (!(x==xPosition && y==yPosition) && ![aTile isDefinitive] && shouldCompare) {
					countPossibility = 0;
					for (i=0; countPossibility<=nrOfPossibitiesToCheck && i<passNeed; i++) {
						if ([aTile containsValue:[[possibilities objectAtIndex:i] intValue]]) {
							countPossibility++;
						}
					}
					if (countPossibility==nrOfPossibitiesToCheck) countPair++;
					if (countPair==passNeed) {
						containsPair=YES;
						location[kLocationInBox] = 1;
					}
				}
			}
		}
		
		countPair = 1;
		for (x=0; x<kTileGridSize; x++) { //Is the value in the row
			SudokuTile *aTile = tiles2D[x][yPosition];
			int nrOfPossibitiesToCheck = [[aTile possibilities] count];
			BOOL shouldCompare = (nrOfPossibitiesToCheck<=passNeed);
			if (x!=xPosition && ![aTile isDefinitive] && shouldCompare) {
				countPossibility = 0;
				for (i=0; countPossibility<=nrOfPossibitiesToCheck && i<passNeed; i++) {
					if ([aTile containsValue:[[possibilities objectAtIndex:i] intValue]]) {
						countPossibility++;
					}
				}
				if (countPossibility==nrOfPossibitiesToCheck) countPair++;
				if (countPair==passNeed) {
					containsPair=YES;
					location[kLocationInRow] = 1;
				}
			}
		}
		
		countPair = 1;
		for (y=0; y<kTileGridSize; y++) { //Is the value in the column
			SudokuTile *aTile = tiles2D[xPosition][y];
			int nrOfPossibitiesToCheck = [[aTile possibilities] count];
			BOOL shouldCompare = (nrOfPossibitiesToCheck<=passNeed);
			if (y!=yPosition && ![aTile isDefinitive] && shouldCompare) {
				countPossibility = 0;
				for (i=0; countPossibility<=nrOfPossibitiesToCheck && i<passNeed; i++) {
					if ([aTile containsValue:[[possibilities objectAtIndex:i] intValue]]) {
						countPossibility++;
					}
				}
				if (countPossibility==nrOfPossibitiesToCheck) countPair++;
				if (countPair==passNeed) {
					containsPair=YES;
					location[kLocationInColumn] = 1;
				}
			}
		}
	}
	
	return containsPair;
}

- (int)removeFromTilesPossibilitiesOfPair:(int)value atX:(int)xPosition andY:(int)yPosition inLocation:(int *)location {
	NSPoint reg = [self indexRegionForTileAtX:xPosition andY:yPosition];
	SudokuTile *maTile = tiles2D[xPosition][yPosition];
	NSArray *possibilities = [maTile possibilities];
	int i, x, y;
	int countPossibility = 0, passNeed = [possibilities count];
	int removed = 0;
	
	if (location[kLocationInBox]==1) {
		for (x=3*reg.x; x<3*reg.x+3; x++) { //Is the pair in the region
			for (y=3*reg.y; y<3*reg.y+3; y++) {
				SudokuTile *aTile = tiles2D[x][y];
				int nrOfPossibitiesToCheck = [[aTile possibilities] count];
				BOOL shouldCompare = (nrOfPossibitiesToCheck<=passNeed);
				countPossibility = 0;
				if (!(x==xPosition && y==yPosition) && ![aTile isDefinitive]) {
					if (shouldCompare) {
						for (i=0; countPossibility<=nrOfPossibitiesToCheck && i<passNeed; i++) {
							if ([aTile containsValue:[[possibilities objectAtIndex:i] intValue]]) {
								countPossibility++;
							}
						}
					}
					if (countPossibility!=nrOfPossibitiesToCheck) {
						removed++;
						[aTile removePossibilitesOf:value];
					}
				}
			}
		}
	}
	
	if (location[kLocationInRow]==1) {
		countPossibility = 0;
		for (x=0; x<kTileGridSize; x++) { //Is the value in the row
			SudokuTile *aTile = tiles2D[x][yPosition];
			int nrOfPossibitiesToCheck = [[aTile possibilities] count];
			BOOL shouldCompare = (nrOfPossibitiesToCheck<=passNeed);
			countPossibility = 0;
			if (x!=xPosition && ![aTile isDefinitive]) {
				/*Is the tile part of pair*/
				if (shouldCompare) {
					for (i=0; countPossibility<=nrOfPossibitiesToCheck && i<passNeed; i++) {
						if ([aTile containsValue:[[possibilities objectAtIndex:i] intValue]]) {
							countPossibility++;
						}
					}
				}
				/**/
				if (countPossibility!=nrOfPossibitiesToCheck) {
					removed++;
					[aTile removePossibilitesOf:value];
				}
			}
		}
	}
	
	if (location[kLocationInColumn]==1) {
		countPossibility = 0;
		for (y=0; y<kTileGridSize; y++) { //Is the value in the column
			SudokuTile *aTile = tiles2D[xPosition][y];
			int nrOfPossibitiesToCheck = [[aTile possibilities] count];
			BOOL shouldCompare = (nrOfPossibitiesToCheck<=passNeed);
			countPossibility = 0;
			if (y!=yPosition && ![aTile isDefinitive]) {
				if (shouldCompare) {
					for (i=0; countPossibility<=nrOfPossibitiesToCheck && i<passNeed; i++) {
						if ([aTile containsValue:[[possibilities objectAtIndex:i] intValue]]) {
							countPossibility++;
						}
					}
				}
				if (countPossibility!=nrOfPossibitiesToCheck) {
					removed++;
					[aTile removePossibilitesOf:value];
				}
			}
		}
	}
	
	return removed;
}

- (BOOL)shouldValidateTileValue:(int)value atX:(int)xPosition andY:(int)yPosition {
	BOOL canValidate=YES;
	NSPoint reg = [self indexRegionForTileAtX:xPosition andY:yPosition];
	int x, y;
		
	for (x=3*reg.x; x<3*reg.x+3; x++) {
		for (y=3*reg.y; y<3*reg.y+3; y++) {
			SudokuTile *aTile = tiles2D[x][y];
			if (!(x==xPosition && y==yPosition) && [aTile containsValue:value])
				canValidate=NO;
		}
	}
	
	for (x=0; !canValidate && x<kTileGridSize; x++) {
		SudokuTile *aTile = tiles2D[x][yPosition];
		if (x!=xPosition && [aTile containsValue:value])
			canValidate=NO;
	}
	
	for (y=0; !canValidate && y<kTileGridSize; y++) {
		SudokuTile *aTile = tiles2D[xPosition][y];
		if (y!=yPosition && [aTile containsValue:value])
			canValidate=NO;
	}
	
	return canValidate;
}

- (void)dealloc {
	//[self releaseTiles];	
	//[tiles release], tiles = nil;
	[sudokuImage release], sudokuImage = nil;
	[super dealloc];
}

- (void)releaseTiles {
	int i,j;
	for (i=0; i<kTileGridSize; i++) {
		for (j=0; j<kTileGridSize; j++) {
			SudokuTile *aTile = tiles2D[i][j];
			[aTile release];
		}
	}
}

@end
