//
//  MyDocument.m
//  Sudoku
//
//  Created by Anthony on 20/08/06.
//  Copyright __MyCompanyName__ 2006 . All rights reserved.
//

#import "MyDocument.h"

@implementation MyDocument

- (id)init 
{
    self = [super init];
    if (self != nil) {
		NSLog(@"Init...");
		didTilesInit = NO;
    }
    return self;
}

- (void)awakeFromNib {
	if (!didTilesInit) {
		NSLog(@"Loading from model...");
		if (![sudokuGrid initTilesFromModel])
			;//[super openDocument:nil];
		didTilesInit = YES;
	}
}

- (NSString *)windowNibName 
{
    return @"MyDocument";
}

- (void)windowControllerDidLoadNib:(NSWindowController *)windowController 
{
    [super windowControllerDidLoadNib:windowController];
    // user interface preparation code
}


/*- (id)managedObjectModel {
	id managedObjectModel = [super managedObjectModel];
	
	return managedObjectModel;
}*/

- (IBAction)saveDocument:sender {
	NSLog(@"Saving sudoku grid...");
	[self saveSodokuContents];
	NSLog(@"Saving to document...");
	[super saveDocument:sender];
}

- (IBAction)saveDocumentAs:sender {
	NSLog(@"Saving sudoku grid...");
	[self saveSodokuContents];
	NSLog(@"Saving to document as...");
	[super saveDocumentAs:sender];
}

- (IBAction)play:sender {
	[sudokuGrid play:sender];
}

- (IBAction)check:sender {
	[sudokuGrid check:sender];
}

- (IBAction)reset:sender {
	[sudokuGrid reset:sender];
}

- (IBAction)showSolution:sender {
	[sudokuGrid showSolution:sender];
}

- (IBAction)showTilesArray:sender {
	[tilesList orderFront:sender];
}

- (void)saveSodokuContents {
	int i;
	NSLog(@"Fetching tiles...");
	NSArray *tiles = [sudokuGrid arrayFromTiles];
	
	NSLog(@"Saving to model...");
	for (i=0; i<[tiles count]; i++) {
		NSManagedObject *newTile;
		SudokuTile *aTile = [tiles objectAtIndex:i];
		newTile = [NSEntityDescription insertNewObjectForEntityForName:@"Tiles" inManagedObjectContext:[self managedObjectContext]];
		[newTile setValue:[NSNumber numberWithInt:[aTile x]] forKey:@"xPosition"];
		[newTile setValue:[NSNumber numberWithInt:[aTile y]] forKey:@"yPosition"];
		[newTile setValue:[NSNumber numberWithBool:[aTile isDefinitive]] forKey:@"isDefinitive"];
		[newTile setValue:[NSNumber numberWithBool:[aTile isPredefined]] forKey:@"isPredefined"];
		[newTile setValue:[NSNumber numberWithInt:[aTile value]] forKey:@"value"];
	}
	
	NSLog(@"Commit the change...");
	// commit the change.
    [[self managedObjectContext] commitEditing];
//    [[[self managedObjectContext] undoManager] removeAllActions];
}

- (void) dealloc {
	[super dealloc];
}

@end
