//
//  MyDocument.h
//  Sudoku
//
//  Created by Anthony on 20/08/06.
//  Copyright __MyCompanyName__ 2006 . All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "SudokuGridView.h"

@interface MyDocument : NSPersistentDocument {
	IBOutlet SudokuGridView			*sudokuGrid;
	IBOutlet NSWindow				*tilesList;
	
	BOOL							didTilesInit;
}

- (IBAction)play:sender;
- (IBAction)check:sender;
- (IBAction)reset:sender;
- (IBAction)showSolution:sender;
- (IBAction)showTilesArray:sender;

- (void)saveSodokuContents;
@end
