//
//  SudokuGridView.h
//  SudokuResolver
//
//  Created by Anthony on 05/08/06.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "SudokuTile.h"
#import "SelectionRingView.h"

enum SudokuLocation {
	kLocationInRow = 0,
	kLocationInColumn,
	kLocationInBox
};

@interface SudokuGridView : NSView {
	IBOutlet id delegate;
	IBOutlet SelectionRingView	*selectionRingView;
	
	NSImage	*sudokuImage;
	BOOL candidateSelectionPending;
	BOOL buildMode;
	NSPoint clickedTile;
	SudokuTile *tiles2D[kTileGridSize][kTileGridSize];
	//NSMutableArray	*tiles;
}


- (void) initTiles;
- (BOOL)initTilesFromModel;
- (NSArray *)arrayFromTiles;
//Converts a location in grid index
- (NSPoint) indexForTileAtPoint:(NSPoint)loc;
- (NSPoint) indexRegionForTileAtX:(int)xPosition andY:(int)yPosition;
- (int) indexRegionForIndex:(int)index;
//Returns a rect for proper tile at x and y
- (NSRect) rectForTileAtX:(int)tileX andY:(int)tileY;
//Draws the proper tile at x and y on the view
- (void) drawTileAtX:(int)xPosition andY:(int)yPosition inRect:(NSRect)rect;
//
- (id)fetchTileAtX:(int)xPosition andY:(int)yPosition;
- (id)executeTileFetch:(NSFetchRequest *)request;

- (IBAction)play:sender;
- (IBAction)check:sender;
- (IBAction)reset:sender;
- (IBAction)showSolution:sender;

- (void)findAllPossibilitiesForTileAtX:(int)xPosition andY:(int)yPosition update:(int *)resolved;
//resolving methods
- (int)resolveBySearchingCandidates;
- (int)resolveByUnicityOfCandidate;
- (int)resolveByUnicityOfPossibility;
- (int)resolveByUnicityOfPossibility:(int *)totalCompleted;
- (int)resolveBySearchingPairs;

- (BOOL)shouldValidateTileValue:(int)value atX:(int)xPosition andY:(int)yPosition;
- (BOOL)shouldInsertTileValue:(int)value atX:(int)xPosition andY:(int)yPosition;
- (BOOL)containsPairOfPossibilities:(int)value atX:(int)xPosition andY:(int)yPosition location:(int *)location;

- (void)removePossibilitiesForAllTiles;
- (int)removeFromTilesPossibilitiesOfPair:(int)value atX:(int)xPosition andY:(int)yPosition inLocation:(int *)location;
- (void)removePossibilityByReferenceOf:(int)value atX:(int)xPosition andY:(int)yPosition;

- (void)prepareForResolution;

- (void)releaseTiles;

- (void)addCandidate:(int)candidate inDraftMode:(BOOL)draftMode;
@end
