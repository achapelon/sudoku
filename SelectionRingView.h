#import <Cocoa/Cocoa.h>

@interface SelectionRingView : NSView {
	IBOutlet NSButton *selectionModeButton;
	BOOL draftMode;
}
- (IBAction)click:(id)sender;
- (IBAction)toggleMode:(id)sender;
@end
