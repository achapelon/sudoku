//
//  SudokuTile.m
//  Sudoku
//
//  Created by Anthony on 19/08/06.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import "SudokuTile.h"


@implementation SudokuTile

+(id)tileAtX:(int)x andY:(int)y {
	return [self tileWithValue:0 atX:x andY:y];
}

+(id)tileWithValue:(int)aValue atX:(int)x andY:(int)y {
	return [self tileWithValue:aValue andIsPredefined:NO atX:x andY:y];
}

+(id)tileWithValue:(int)aValue andIsPredefined:(BOOL)isPredefined atX:(int)x andY:(int)y {
	return [self tileWithValue:aValue isDefintive:isPredefined andIsPredefined:isPredefined atX:x andY:y];
}

+(id)tileWithValue:(int)aValue andIsDefinitive:(BOOL)isDefinitive atX:(int)x andY:(int)y {
	return [self tileWithValue:aValue isDefintive:isDefinitive andIsPredefined:NO atX:x andY:y];
}

+(id)tileWithValue:(int)aValue isDefintive:(BOOL)isDefinitive andIsPredefined:(BOOL)isPredefined atX:(int)x andY:(int)y {
	self = [[super alloc] initWithValue:aValue isDefintive:isDefinitive andIsPredefined:isPredefined atX:x andY:y];
	
	return self;
}

-(id)initWithValue:(int)aValue isDefintive:(BOOL)isDefinitive andIsPredefined:(BOOL)isPredefined atX:(int)x andY:(int)y {
	self = [super init];
	if (self) {
		_x = x;
		_y = y;
		_value = aValue;
		_isDefinitive = isDefinitive;
		_isPredefined = isPredefined;
		_isIncorrect = NO;
		
		int i;
		for (i=1; i<=kNumberPossibilities; i++)
			_possibilities[i] = NO;
	}
	
	return self;
}

-(int)x {
	return _x;
}

-(int)y {
	return _y;
}


-(int)value {
	return _value;
}

-(BOOL)isPredefined {
	return _isPredefined;
}

-(BOOL)isDefinitive {
	return _isDefinitive;
}

-(BOOL)isIncorrect {
	return _isIncorrect;
}

-(NSArray *)possibilities {
	NSMutableArray *possibilities = [NSMutableArray array];
	int i;
	if (_value>0) {
		NSString *str = [NSString stringWithFormat:@"%d", _value];
		for (i=0; i<[str length]; i++) {
			NSNumber *possibility = [NSNumber numberWithInt:[[str substringWithRange:NSMakeRange(i,1)] intValue]];
			[possibilities addObject:possibility];
		}
	} else {
		for (i=0; i<kTileGridSize; i++) {
			NSNumber *possibility = [NSNumber numberWithInt:i+1];
			[possibilities addObject:possibility];
		}
	}
	
	return possibilities;
}	
	
-(void)insertValue:(int)aValue {
	[self insertValue:aValue andIsDefinitive:NO];
}

-(void)insertValue:(int)aValue andIsDefinitive:(BOOL)isDefinitive {
	[self insertValue:aValue isDefinitive:isDefinitive andIsPredefined:NO];
}

-(void)insertValue:(int)aValue andIsPredefined:(BOOL)isPredefined {
	[self insertValue:aValue isDefinitive:isPredefined andIsPredefined:isPredefined];
}

-(void)insertValue:(int)aValue isDefinitive:(BOOL)isDefinitive andIsPredefined:(BOOL)isPredefined {
	int value = _value;
	if (isDefinitive || isPredefined) {
		value = aValue;
	} else {
		NSString *str = [NSString stringWithFormat:@"%d", value];
		NSRange range = [str rangeOfString:[NSString stringWithFormat:@"%d", aValue]];
		NSRange stringNotFound = NSMakeRange(NSNotFound,0);
		if ( NSEqualRanges(range,stringNotFound) )
			str = [NSString stringWithFormat:@"%d%d", value, aValue];
		value = [str intValue];
		if ( !isDefinitive ) _possibilities[aValue] = YES;
	}
	[self setValue:value isDefinitive:isDefinitive andIsPredefined:isPredefined];
}

-(void)setValue:(int)aValue {
	[self setValue:aValue andIsDefinitive:NO];
}

-(void)setValue:(int)aValue andIsDefinitive:(BOOL)isDefinitive {
	[self setValue:aValue isDefinitive:isDefinitive andIsPredefined:NO];
}

-(void)setValue:(int)aValue andIsPredefined:(BOOL)isPredefined {
	[self setValue:aValue isDefinitive:isPredefined andIsPredefined:isPredefined];
}

-(void)setValue:(int)aValue isDefinitive:(BOOL)isDefinitive andIsPredefined:(BOOL)isPredefined {
	_value = aValue;
	[self setDefinitive:isDefinitive];
	[self setPredefined:isPredefined];
	[self setIncorrect:NO];
	
	if ( aValue==0 ) {
		int i;
		for (i=1; i<=kNumberPossibilities; i++)
			_possibilities[i] = NO;
	}
}

-(void)setPredefined:(BOOL)isPredefined {
	_isPredefined = isPredefined;
}

-(void)setDefinitive:(BOOL)isDefinitive {
	_isDefinitive = isDefinitive;
}

-(void)setIncorrect:(BOOL)isIncorrect {
	_isIncorrect = isIncorrect;
}

-(void)removePossibilitesOf:(int)value {
	NSMutableArray *possibilities = [NSMutableArray array];
	NSMutableArray *possibilities2 = [NSMutableArray array];
	int i;
	
	NSString *str = [NSString stringWithFormat:@"%d", _value];
	for (i=0; i<[str length]; i++) {
		NSNumber *possibility = [NSNumber numberWithInt:[[str substringWithRange:NSMakeRange(i,1)] intValue]];
		[possibilities addObject:possibility];
	}
	
	NSString *str2 = [NSString stringWithFormat:@"%d", value];
	for (i=0; i<[str2 length]; i++) {
		NSNumber *possibility = [NSNumber numberWithInt:[[str2 substringWithRange:NSMakeRange(i,1)] intValue]];
		[possibilities2 addObject:possibility];
	}
	
	for(i=0; i<[possibilities2 count]; i++)
		[possibilities removeObject:[possibilities2 objectAtIndex:i]];
	
	_value = 0;
	for(i=0; i<[possibilities count]; i++) 
		[self insertValue:[[possibilities objectAtIndex:i] intValue]];
	
	_possibilities[value] = NO;
}

-(BOOL)containsValue:(int)aValue {
	BOOL contains = NO;
	NSString *str = [NSString stringWithFormat:@"%d", _value];
	NSRange range = [str rangeOfString:[NSString stringWithFormat:@"%d", aValue]];
	NSRange stringNotFound = NSMakeRange(NSNotFound,0);
	if ( !NSEqualRanges(range,stringNotFound) ) contains = YES;
	if ( _possibilities[aValue]!=contains )
		NSLog ( @"Possibilities=%d ; Possibility=%d %d %d\n", _value, aValue, _possibilities[aValue], contains);	
	return contains;

}

-(NSString *)description {
	NSString *desc = [NSString stringWithFormat:@"{ x = %d; y = %d; value = %d; isPredefined = %d; isDefinitive = %d }", _x, _y, _value, _isPredefined, _isDefinitive];
	return desc;
}

-(void)dealloc {
	[super dealloc];
}

@end
